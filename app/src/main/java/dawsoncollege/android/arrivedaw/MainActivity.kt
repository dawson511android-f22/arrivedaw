package dawsoncollege.android.arrivedaw

import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.zxing.BarcodeFormat
import com.google.zxing.WriterException
import com.google.zxing.common.BitMatrix
import com.google.zxing.qrcode.QRCodeWriter
import dawsoncollege.android.arrivedaw.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val QRtest = "Hello this is a test string! It will be used to generate a QR code"
        generateQR(QRtest);

        }

    private fun generateQR(str: String) {
        val bitmap: Bitmap? = encodeStringToBitmap(str)
        binding.QRResult.setImageBitmap(bitmap)
        binding.QRResult.visibility = View.VISIBLE
    }

    @Throws(WriterException::class)
    fun encodeStringToBitmap(str: String?): Bitmap? {
        val writer = QRCodeWriter()
        val bitMatrix: BitMatrix = writer.encode(str, BarcodeFormat.QR_CODE, 400, 400)
        val w = bitMatrix.width
        val h = bitMatrix.height
        val pixels = IntArray(w * h)
        for (y in 0 until h) {
            var offset = y*w
            for (x in 0 until w) {
                if(bitMatrix[x, y]){
                    pixels[offset + x] = Color.BLACK
                }else{
                    pixels[offset + x] = Color.WHITE
                }
            }
        }
        val bitmap:Bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)

        bitmap.setPixels(pixels, 0, w, 0, 0, w, h)
        return bitmap
    }


}